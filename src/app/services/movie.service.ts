import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MovieAddModel } from '../models/movie-add.model';
import { MovieDetailsModel } from '../models/movie-details.model';
import { MovieIndexModel } from '../models/movie-index.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(
    private http: HttpClient
  ) { }

  getIndex() {
    return this.http.get<MovieIndexModel[]>("http://localhost:35656/api/movie?limit=100");
  }

  getDetails(id: number) {
    return this.http.get<MovieDetailsModel>("http://localhost:35656/api/movie/" + id);
  }

  addMovie(model: MovieAddModel) {
    return this.http.post("http://localhost:35656/api/movie", model);
  }
}
