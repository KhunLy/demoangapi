export interface MovieAddModel {
  title: string;
  duration: number;
  categoryId: number;
  poster: string;
}