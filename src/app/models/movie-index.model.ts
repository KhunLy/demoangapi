export interface MovieIndexModel {
    id: number;
    title: string;
    poster: string;
}