export interface MovieDetailsModel {
  id: number;
  title: string;
  synopsis: string;
  poster: string;
  duration: number;
  categoryId: number;
  categoryName: string;
}