import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieDetailsModel } from 'src/app/models/movie-details.model';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  model!: MovieDetailsModel

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;
    this.movieService.getDetails(id).subscribe(data => this.model = data);
  }

}
