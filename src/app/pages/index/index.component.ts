import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MovieIndexModel } from 'src/app/models/movie-index.model';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  model!: MovieIndexModel[];

  fg!: FormGroup;

  constructor(
    private movieService: MovieService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.movieService.getIndex().subscribe(data => this.model = data);
    this.fg = this.fb.group({
      title: [null],
      poster: [null],
      duration: [null],
      synopsis: [null],
    })
  }

  submit() {
    this.movieService.addMovie(this.fg.value).subscribe(() => {
      this.movieService.getIndex().subscribe(data => this.model = data);
    });
  }

}
